.PHONY: smoke clean

SMOKE_TEST_SCRIPT=scripts/smoke_test
SMOKE_TEST_IMAGE=python:2.7

SMOKE_TEST_VOLUMES=-v $(LOCAL_WORKSPACE_DIR)/tmp/mypythoncli:/var/workspace  -v $(LOCAL_WORKSPACE_DIR)/$(SMOKE_TEST_SCRIPT):/var/workspace/smoke_test
SMOKE_TEST_OPTIONS=--rm $(SMOKE_TEST_VOLUMES) --entrypoint /bin/sh -w /var/workspace

smoke:
	@mkdir -p ./tmp
	@cp -R ./scripts ./tmp
	@cookiecutter ./ --no-input --output-dir ./tmp 
	@docker run $(SMOKE_TEST_OPTIONS) $(SMOKE_TEST_IMAGE) smoke_test

clean:
	@rm -rf ./tmp
