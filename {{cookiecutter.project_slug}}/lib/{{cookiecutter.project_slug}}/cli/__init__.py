"""cli module"""

import {{cookiecutter.project_slug}}.commands
import {{cookiecutter.project_slug}}.shell
from {{cookiecutter.project_slug}}.core import Environment

def initialize(ctx=None):
    """initialize module"""
    ctx = Environment()

    {{cookiecutter.project_slug}}.commands.initialize(ctx)
    {{cookiecutter.project_slug}}.shell.initialize(ctx)
