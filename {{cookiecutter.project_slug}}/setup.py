#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""
import sys
import os

project_root = os.getcwd()
lib_root = project_root + "/lib"
sys.path.insert(0, lib_root)

from setuptools import setup, find_packages
from {{cookiecutter.project_slug}} import VERSION as version

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.rst') as history_file:
    history = history_file.read()

requirements = [
    'Click',
    'ostruct'
]

setup_requirements = [
]

test_requirements = [
    'nose'
]

setup(
    name='{{cookiecutter.project_slug}}',
    version=version,
    description="{{cookiecutter.project_summary}}",
    long_description=readme + '\n\n' + history,
    author="{{cookiecutter.author_name}}",
    author_email='{{cookiecutter.author_email}}',
    url='{{cookiecutter.project_home}}',
    package_dir={ '': 'lib'},
    packages=find_packages('lib'),
    entry_points='''
        [console_scripts]
        {{cookiecutter.project_slug}}={{cookiecutter.project_slug}}.cli:initialize
    ''',
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='test',
    tests_require=test_requirements,
    setup_requires=setup_requirements,
)
