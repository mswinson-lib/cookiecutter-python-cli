# cookiecutter-python-cli

    cookiecutter generator a simple python console app


## Installation

    git clone http://bitbucket.org/mswinson-lib/cookiecutter-python-cli


## Usage

    cookiecutter ./cookiecutter-python-cli


## Contributing

1. Fork it ( https://bitbucket.org/mswinson-lib/cookiecutter-python-cli/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new pull request
