"""shell module"""

import click

class Shell(click.Group):
    """shell container"""
    pass

def initialize(ctx):
    """initialize module"""
    cli = Shell()

    commands = ctx.commands
    for command in commands:
        cli.add_command(command)

    cli()
