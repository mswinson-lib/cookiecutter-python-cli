#!/usr/bin/env bash
PROJECT_NAME=$1
ARTIFACT_DIR=$2
twine upload ${ARTIFACT_DIR}/dist/*
