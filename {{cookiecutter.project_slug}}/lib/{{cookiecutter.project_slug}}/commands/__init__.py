"""commands"""

from .version import version

def initialize(ctx):
    """initialize module"""
    ctx.commands = []
    ctx.commands.append(version)
