#!/usr/bin/env bash
ARTIFACT_DIR=$1

python setup.py sdist
python setup.py bdist_wheel
mkdir -p ${ARTIFACT_DIR}/dist
cp -R dist/* ${ARTIFACT_DIR}/dist

rm -fr build/
rm -fr dist/
rm -fr .eggs/
find . -name '*.egg-info' -exec rm -fr {} +
find . -name '*.egg' -exec rm -f {} +

