"""environment"""

from ostruct import OpenStruct

class Environment(OpenStruct):
    """open container for environment variables"""
    pass
