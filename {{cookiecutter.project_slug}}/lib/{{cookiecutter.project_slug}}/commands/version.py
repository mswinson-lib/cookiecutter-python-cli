"""version command"""

import click
import {{cookiecutter.project_slug}}

@click.command("version", short_help="show version information")
def version():
    """version command"""
    click.echo({{cookiecutter.project_slug}}.VERSION)
