wheel==0.29.0
watchdog==0.8.3
pylint
nose
coverage==4.1
sphinx
twine
