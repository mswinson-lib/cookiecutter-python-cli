=========================================
{{cookiecutter.project_name}}
=========================================

{{cookiecutter.project_summary}}

.. image:: https://readthedocs.org/projects/{{cookiecutter.project_slug}}/badge/?version=latest
  :target: http://{{cookiecutter.project_slug}}.readthedocs.io/en/latest/?badge=latest
  :alt: Documentation Status

      
=============
Installation
=============

    pip install {{cookiecutter.project_slug}}

=============
Usage
=============

    import {{cookiecutter.project_slug}}


===============
Contributing
===============

1. Fork it ( {{cookiecutter.project_home}}/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new pull request
